package br.ufla.dcc.sd.rmi.clock.client;

import java.rmi.RemoteException;
import java.sql.Date;
import java.util.Random;

import br.ufla.dcc.sd.rmi.clock.remote.Clock;

public class ClockClient implements Clock {
	private long difference;
	private long erro;
	private Random random;

	public ClockClient() throws RemoteException {
		randonize();
	}

	@Override
	public Long getTime() throws RemoteException {
		return (System.currentTimeMillis() + erro);
	}

	@Override
	public void setTime(long time) throws RemoteException {
		this.erro += time;
	}

	@Override
	public Long getDifference() throws RemoteException {
		return this.difference;
	}

	@Override
	public void setDifference(Long diff) throws RemoteException {
		this.difference = diff;
	}

	@Override
	public final void randonize() throws RemoteException {
		this.random = new Random();
		this.erro = random.nextInt(30) * 1000;
	}

	@Override
	public Date getDate() {
		return new Date(System.currentTimeMillis() + this.erro);
	}

}
