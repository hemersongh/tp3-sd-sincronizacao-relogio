package br.ufla.dcc.sd.rmi.clock.remote;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.Date;

public interface Clock extends Remote {
	public Long getTime() throws RemoteException;

	public void setTime(long time) throws RemoteException;

	public Long getDifference() throws RemoteException;

	public void setDifference(Long diff) throws RemoteException;

	public void randonize() throws RemoteException;

	public Date getDate() throws RemoteException;
}