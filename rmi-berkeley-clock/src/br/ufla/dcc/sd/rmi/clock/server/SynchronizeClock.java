package br.ufla.dcc.sd.rmi.clock.server;

import java.net.SocketException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Scanner;

import br.ufla.dcc.sd.rmi.clock.berkeley.Berkeley;
import br.ufla.dcc.sd.rmi.clock.remote.Clock;

public class SynchronizeClock {
	public static ClockServer clock;
	public static ArrayList<Clock> servers = new ArrayList<>();
	public static final int PORT = 7500;
	public static final int PORT_SERVER = 7550;

	private static void registerClient(String address) throws RemoteException, NotBoundException {
		Registry registry = LocateRegistry.getRegistry(address, PORT);

		servers.add((Clock) registry.lookup("Clock"));
	}

	private static void registerServer() throws RemoteException, NotBoundException, SocketException {
		clock = new ClockServer();

		clock.initializeClock(PORT_SERVER);
		servers.add(clock);
	}

	private static void syncronizeServers() throws RemoteException {
		System.out.println("\n" + "--------------------------------------------------");
		System.out.println("---------- Starting synchronization... -----------");

		servers = Berkeley.fastSync(servers);

		System.out.println("---------------- Synchronizing!!! ----------------");
		System.out.println("--------------------------------------------------");
		System.out.print("---------------- New synced times: ---------------");

		showServers();
	}

	private static void randonizeServers() throws RemoteException {
		System.out.println("\n" + "--------------------------------------------------");
		System.out.println("--------- Randomizing servers clocks... ----------");

		for (Clock server : servers) {
			server.randonize();
		}

		System.out.println("--------------------------------------------------");
		System.out.print("---------------- New times random ----------------");

		showServers();
	}

	private static void showServers() throws RemoteException {
		System.out.println("\n" + "--------------------------------------------------");

		for (int i = 0; i < servers.size(); i++) {
			System.out.println("-------- Server (" + (i + 1) + ") - Clock: " + servers.get(i).getTime() + " -------");
			System.out.println("--------              Date: " + servers.get(i).getDate() + "     -------");
			System.out.println("--------------------------------------------------");
		}
	}

	private static void executeAction(int action) throws RemoteException {
		switch (action) {
		case 0:
			System.out.println("\n" + "----------------- ...Goodbye... ------------------");
			System.exit(1);
		case 1:
			showServers();
			break;
		case 2:
			randonizeServers();
			break;
		case 3:
			syncronizeServers();
			break;
		default:
			System.out.println("Unknown action, try again...");
		}
	}

	private static void showMenu() {
		System.out.println("\n" + "#################### Choose an action: ");
		System.out.println("########## 1 - Show clocks");
		System.out.println("########## 2 - Randomize clocks");
		System.out.println("########## 3 - Synchronize clocks");
		System.out.print("########## -> ");
	}

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		int action;
		String[] addresses = { "192.168.25.130" };

		addresses = (args.length == 0) ? addresses : args;

		Scanner scan = new Scanner(System.in);

		System.out.println("##################################################");

		try {
			registerServer();

			for (String address : addresses) {
				registerClient(address);
			}

			System.out.println("############# Starting coordinator... #############");

			showMenu();

			while (scan.hasNext()) {
				action = scan.nextInt();

				executeAction(action);

				System.out.print("\n" + "Enter with the next action: ");
			}
		} catch (NotBoundException | RemoteException nbre) {
			System.err.println("Error: " + nbre.getMessage());
			nbre.printStackTrace();
		} catch (SocketException se) {
			se.printStackTrace();
		}

		System.out.println("##################################################");
	}
}