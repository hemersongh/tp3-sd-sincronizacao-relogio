package br.ufla.dcc.sd.rmi.clock.berkeley;

import java.rmi.RemoteException;
import java.util.ArrayList;

import br.ufla.dcc.sd.rmi.clock.remote.Clock;

public class Berkeley {
	private int average;
	private int total;
	private ArrayList<Clock> servers;

	public Berkeley(ArrayList<Clock> servers) {
		this.servers = servers;
	}

	public void syncronize() throws RemoteException {
		average = total = 0;

		for (Clock server : this.servers) {
			server.setDifference(server.getTime() - this.servers.get(0).getTime());

			total += server.getDifference();
		}

		average = total / this.servers.size();

		Long newTime = null;

		for (Clock server : this.servers) {
			newTime = average + (server.getDifference() * (-1));

			server.setTime(newTime);
		}
	}

	public void syncronize(ArrayList<Clock> servers) throws RemoteException {
		setServers(servers);
		syncronize();
	}

	public static ArrayList<Clock> fastSync(ArrayList<Clock> servers) throws RemoteException {
		Berkeley berkeley = new Berkeley(servers);

		berkeley.syncronize();

		return berkeley.getServers();
	}

	public ArrayList<Clock> getServers() {
		return this.servers;
	}

	public void setServers(ArrayList<Clock> servers) {
		this.servers = servers;
	}
}