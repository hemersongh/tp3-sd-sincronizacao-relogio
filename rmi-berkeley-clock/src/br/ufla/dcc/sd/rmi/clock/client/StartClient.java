package br.ufla.dcc.sd.rmi.clock.client;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import br.ufla.dcc.sd.rmi.clock.remote.Clock;

public class StartClient {
	public static final int PORT = 7500;

//	public static String getIP() {
//		@SuppressWarnings("rawtypes")
//		Enumeration e = null;
//		
//		try {
//			e = NetworkInterface.getNetworkInterfaces();
//		} catch (SocketException se) {
//			se.printStackTrace();
//		}
//		
//		NetworkInterface ni = (NetworkInterface) e.nextElement();
//
//		e = ni.getInetAddresses();
//		
//		InetAddress i = (InetAddress) e.nextElement();
//		
//		i = (InetAddress) e.nextElement();
//
//		return i.getHostAddress().toString();
//	}

	public static void main(String[] args) {
//		String ip = getIP();

		try {
			System.setProperty("java.rmi.server.hostname", "192.168.25.130");

			ClockClient server = new ClockClient();
			Clock stub = (Clock) UnicastRemoteObject.exportObject(server, 0);
			Registry reg = LocateRegistry.createRegistry(PORT);

			reg.rebind("Clock", stub);

			System.out.println("Client clock ready!");
		} catch (RemoteException re) {
			System.err.println("Error: " + re.getMessage());
			re.printStackTrace();
		}
	}
}