package br.ufla.dcc.sd.rmi.clock.server;

import java.net.SocketException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import br.ufla.dcc.sd.rmi.clock.client.ClockClient;
import br.ufla.dcc.sd.rmi.clock.remote.Clock;

public class ClockServer extends ClockClient {

	public ClockServer() throws RemoteException {
		super();
	}

//	public String getIP() {
//		@SuppressWarnings("rawtypes")
//		Enumeration e = null;
//		
//		try {
//			e = NetworkInterface.getNetworkInterfaces();
//		} catch (SocketException e1) {
//			e1.printStackTrace();
//		}
//		
//		NetworkInterface ni = (NetworkInterface) e.nextElement();
//
//		e = ni.getInetAddresses();
//		
//		InetAddress i = (InetAddress) e.nextElement();
//		
//		i = (InetAddress) e.nextElement();
//
//		return i.getHostAddress().toString();
//	}

	public void initializeClock(int port) throws SocketException {
//		String ip = getIP();

		try {
			System.setProperty("java.rmi.server.hostname", "192.168.25.62");

			Clock stub = (Clock) UnicastRemoteObject.exportObject(this, 0);
			Registry registry = LocateRegistry.createRegistry(port);

			registry.rebind("Clock", stub);

			System.out.println("Server clock ready!");
			System.out.println("##################################################" + "\n");
		} catch (RemoteException re) {
			System.err.println("Error: " + re.getMessage());
			re.printStackTrace();
		}
	}
}